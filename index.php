<?php

include "class/personne.class.php";
include "class/collection.class.php";
include "class/formateur.class.php";
include "class/formation.class.php";

if (! isset($collectionpersonne)){
    include "init_class.php";
}


if (!isset($_REQUEST['uc']))
    $uc = 'accueil';
else
    $uc = $_REQUEST['uc'];

switch ($uc) {
    case 'accueil':
        {
            include "controllers/c_main.php";
            break;
        }
    case 'traitement':
        {
            include "controllers/c_reception_tab.php";
            break;
        }
}
?>