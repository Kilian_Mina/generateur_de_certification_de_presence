<?php

$html = "
<!DOCTYPE html>
<html>
    <title>Certificat de présence</title>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    
    <body>
        
        <br><br><br><br>
        <div>
            <div><strong>".$this->Formateur->getSex()." ".$this->Formateur->getNom()." ".$this->Formateur->getPrenom()."</strong><br>Adresse<br>Code postal/ ville<br>N°Tél</div>
            
            <div style=\"text-align: right;\"><strong>".$this->Formater->getSex()." ".$this->Formater->getNom()." ".$this->Formater->getPrenom()."</strong><br>Adresse destinataire<br>Code
                postal / ville
            </div>
            
            <br><br><br><br><a></a><br>
            <p > &nbsp;</p>
            <p ><strong><span >ATTESTATION DE PRESENCE</span></strong></p>
            <p style=\"text-align: justify\">
                Je soussigné, ".$this->Formateur->getSex()." ".$this->Formateur->getNom()." ".$this->Formateur->getPrenom().",
                agissant en qualité de ".$this->Formateur->getStatus()." de ".$this->Formation->getNomEtab().",<br/> 
                organisme déclaré auprès de la préfecture de la région ………………………………......... sous le numéro [n° de déclaration d’activité], <br/><br/>
                certifie que : ".$this->Formater->getSex()." ".$this->Formater->getNom()." ".$this->Formater->getPrenom()."a régulièrement suivi l’action de formation suivante : 
                <br/><br/>
                <strong>Date de la formation </strong>: ".$this->Formation->getDteDebutForm()." / ".$this->Formation->getDteFinForm()."<br/>
                <strong>Lieu de réalisation de la formation </strong>: ".$this->Formation->getNomLieuForm()."<br/>
                <strong>Durée en heures </strong>: ".$this->Formation->getTempsTotalForm()."
                <br/><br/><br/>              
                Fait à  , le ".date("d/m/Y - H:i:s")."<br/>
                [Nom, prénom, qualité et signature du formateur ou du représentant de l’organisme]<br/>
                [Cachet de l’organisme]
            </p>

        </div>
    </body>
</html>

";
?>

