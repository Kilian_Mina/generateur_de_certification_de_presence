<?php
/**
 * Initialisation des personne.class et insertion dans la collection
 */

$p1 = new personne("Jean","Jack","M");
$p2 = new personne("Rivière","Marc","M");
$p3 = new personne("Dès","Henri","M");
$p4 = new personne("Vidot","Franck","M");
$p5 = new personne("Poireau","Michael","M");

$p6 = new personne("Conny","Josette","F");
$p7 = new personne("Lefèvre","Clarisse","F");
$p8 = new personne("Morin","Marie","F");
$p9 = new personne("Rousseau","Kassidy","F");
$p10 = new personne("Grondin","Coline","F");



$collectionpersonne = new collection();


$collectionpersonne->addPersonne($p1);
$collectionpersonne->addPersonne($p2);
$collectionpersonne->addPersonne($p3);
$collectionpersonne->addPersonne($p4);
$collectionpersonne->addPersonne($p5);
$collectionpersonne->addPersonne($p6);
$collectionpersonne->addPersonne($p7);
$collectionpersonne->addPersonne($p8);
$collectionpersonne->addPersonne($p9);
$collectionpersonne->addPersonne($p10);


$formateurFrancis = new formateur("Mraty","Francis","M", "Professeur de droit");

$formation1257  = new formation("EDA", "Gymnasium", "18 allée des papillons licorne", "0692587946", "26/05/2018", "30/05/2018", 8);