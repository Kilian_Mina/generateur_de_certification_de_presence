<?php
/**
 * Created by PhpStorm.
 * User: kilian
 * Date: 14/06/18
 * Time: 10:19
 */

class formateur
{
    private $nom;
    private $prenom;
    private $sex;
    private $status;

    /**
     * Constructeur
     * Initialisation des valeurs
     */
    public function __construct($LeNom, $LePrenom, $LeSex, $LeStatus){

        $this->nom = $LeNom;
        $this->prenom = $LePrenom;
        $this->sex = $LeSex;
        $this->status = $LeStatus;
    }


    /**
     * @return le nom de la personne.class
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return le prénom de la personne.class
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return le sex de la personne.class
     */
    public function getSex()
    {
        return $this->sex;
    }
}