<?php
/**
 * Création de personne.class
 */

class personne
{

    private $nom;
    private $prenom;
    private $sex;
    private $presence;

    /**
     * Constructeur
     * Initialisation des valeurs
     */
    public function __construct($LeNom, $LePrenom, $LeSex, $LaPresence = false){

        $this->nom = $LeNom;
        $this->prenom = $LePrenom;
        $this->sex = $LeSex;
        $this->presence = $LaPresence;
    }


    /**
     * @return le nom de la personne.class
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return le prénom de la personne.class
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @return la présence de la personne.class
     */
    public function getPresence()
    {
        return $this->presence;
    }

    /**
     * @param modifie la presence
     */
    public function setPresence($presence)
    {
        $this->presence = $presence;
    }

    /**
     * @return le sex de la personne.class
     */
    public function getSex()
    {
        return $this->sex;
    }


}