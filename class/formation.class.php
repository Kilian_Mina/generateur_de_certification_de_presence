<?php
/**
 * Created by PhpStorm.
 * User: kilian
 * Date: 14/06/18
 * Time: 10:03
 */

class formation
{

    private $NomEtab;
    private $NomLieuForm;
    private $AdrLieu;
    private $Tel;
    private $DteDebutForm;
    private $DteFinForm;
    private $TempsTotalForm;

    public function __construct($nomEtablissement, $nomLieuFormation, $adresseLieu, $telephone, $dteDeb, $dteFin, $tempsTotalFormation)
    {
        $this->NomEtab = $nomEtablissement;
        $this->NomLieuForm = $nomLieuFormation;
        $this->AdrLieu = $adresseLieu;
        $this->Tel = $telephone;
        $this->DteDebutForm = $dteDeb;
        $this->DteFinForm = $dteFin;
        $this->TempsTotalForm = $tempsTotalFormation;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }


    /**
     * @return mixed
     */
    public function getAdrLieu()
    {
        return $this->AdrLieu;
    }

    /**
     * @return mixed
     */
    public function getDteDebutForm()
    {
        return $this->DteDebutForm;
    }

    /**
     * @return mixed
     */
    public function getDteFinForm()
    {
        return $this->DteFinForm;
    }

    /**
     * @return mixed
     */
    public function getNomEtab()
    {
        return $this->NomEtab;
    }

    /**
     * @return mixed
     */
    public function getNomLieuForm()
    {
        return $this->NomLieuForm;
    }

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->Tel;
    }

    /**
     * @return mixed
     */
    public function getTempsTotalForm()
    {
        return $this->TempsTotalForm;
    }
}