<?php
/**
 * Classe régroupant les personnes ayant été présent lors de la formation
 */

class collection
{
    private $Personnes = array();

    public function addPersonne($obj)
    {
        $this->Personnes[] = $obj;
    }

    public function deletePersonne($key)
    {
        if (isset($this->Personnes[$key])) {
            unset($this->Personnes[$key]);
        } else {
            throw new KeyInvalidException("Clé $key invalide.");
        }
    }

    public function getCollection(){
        return $this->Personnes;
    }

    public function getPersonne($key)
    {
        if (isset($this->Personnes[$key])) {
            return $this->Personnes[$key];
        } else {
            throw new KeyInvalidException("Clé $key invalide.");
        }
    }

    public function keys() {
        return array_keys($this->Personnes);
    }

    public function length() {
        return count($this->Personnes);
    }

    public function keyExists($key) {
        return isset($this->Personnes[$key]);
    }
}