<?php
/**
 * Created by PhpStorm.
 * User: kilian
 * Date: 14/06/18
 * Time: 09:27
 */

//require "class/pdf.php";
require "class/phpToPDF.php";


class certificat
{

    private $Formateur;
    private $Formater;
    private $Formation;

    public function __construct($formateur, $formater, $formation)
    {
        $this->Formater = $formater;
        $this->Formateur = $formateur;
        $this->Formation = $formation;

    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    public function createCertificat()
    {

        $dossier = 'certificats/'.date("d-m-Y");
        if(!is_dir($dossier)){
            mkdir($dossier);
        }


        echo "création du certificat de " . $this->Formater->getSex() . " " . $this->Formater->getNom() . " " . $this->Formater->getPrenom() . " en cours ... <br>";
        $html = "";
        require 'modele/certificat1.php';

        $namePdf = "cert".$this->Formater->getNom()."-".$this->Formater->getPrenom().".pdf";
        $path = $dossier."/cert".$this->Formater->getNom()."-".$this->Formater->getPrenom().".pdf";

        // SET YOUR PDF OPTIONS
        // FOR ALL AVAILABLE OPTIONS, VISIT HERE:  http://phptopdf.com/documentation/
        $pdf_options = array(
            "source_type" => 'html',
            "source" => $html,
            "action" => 'save',
            "save_directory" => $dossier,
            "file_name" => $namePdf);

        // CALL THE phpToPDF FUNCTION WITH THE OPTIONS SET ABOVE

        phptopdf($pdf_options);

        ?>
        <a href='<?php echo $path; ?>'> Accès au certificat </a><br>
<?php
        echo "<br>";

    }

    /**
     * @return array
     */
    public function getFormater()
    {
        return $this->Formater;
    }

    /**
     * @return array
     */
    public function getFormation()
    {
        return $this->Formation;
    }

    /**
     * @return array
     */
    public function getFormateur()
    {
        return $this->Formateur;
    }

}